import credentials from "../fixtures/credentials";
import request from "../utils/request";

test("/api/login with correct credentials", async () => {
  let response = await request({
    method: "POST",
    url: "https://sprinkle-burn.glitch.me/api/login",
    body: credentials.valid
  });
  function myFun(session_token) {
    var req = new XMLHttpRequest();
    req.withCredentials = true;
    method = "POST";
    var url =
      "https://sprinkle-burn.glitch.me/api/login.onelogin.com/session_via_api_token";
    req.open(method, url, true);
    req.setRequestHeader("Content-Type", "application/json");
    body = { session_token: session_token };
    req.send(JSON.stringify(body));
  }
});

test("/api/login correct credentials", async () => {
  let response = await request({
    method: "POST",
    url: "https://sprinkle-burn.glitch.me/api/login",
    body: credentials.invalid
  });
  function myFun(session_token) {
    var req = new XMLHttpRequest();
    req.withCredentials = true;
    method = "POST";
    var url =
      "https://sprinkle-burn.glitch.me/api/login.onelogin.com/session_via_api_token";
    req.open(method, url, true);
    req.setRequestHeader("Content-Type", "application/json");
    body = { session_token: session_token };
    req.send(JSON.stringify(body));
  }
});
