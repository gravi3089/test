import { Selector } from "testcafe";
import credentials from "../fixtures/credentials";

fixture`Login Page`.page`https://sprinkle-burn.glitch.me/`;
const email = Selector(".w-100");
const password = Selector("#login-form > fieldset > div:nth-child(3) > input");
const login_btn = Selector("#login-form > fieldset > div.flex > button");
const text = Selector("body > article");
test("Can login with correct credentials", async t => {
  await t
    .typeText(email, credentials.valid.email)
    .typeText(password, credentials.valid.password)
    .click(login_btn);
  await t.expect(Selector(".black-80").innerText).eql("\nWelcome Dr I Test\n");
});

test("Can not login with in-correct credentials", async t => {
  await t
    .typeText(email, credentials.invalid.email)
    .typeText(password, credentials.invalid.password)
    .click(login_btn);
  await t.expect(Selector(".black-80").innerText).eql("\nWelcome Dr I Test\n");
});
